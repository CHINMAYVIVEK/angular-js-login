var App = angular.module('AppDemo', ['ngRoute','socialLogin']);
App.config(function(socialProvider){
    socialProvider.setGoogleKey("Tu GOOGLE ID");
    //socialProvider.setLinkedInKey("YOUR LINKEDIN CLIENT ID");
    socialProvider.setFbKey({appId: "APP ID", apiVersion: "API VERSION"});
});